import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationManager
{
    /**
     * Our configurations stored in this object.
     */
    private final Properties properties;

    public ConfigurationManager(String path)
    {
        this.properties = new Properties();

        try
        {
            reload();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Reloads the settings from the config file.
     * @throws Exception
     */
    void reload() throws Exception
    {
        InputStream input = null;

        try {
            File f = new File("config.ini");
            input = new FileInputStream(f);

            this.properties.load(input);

        } catch (IOException ex) {

            ex.printStackTrace();

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Gets the string value for a specific key.
     * @param key The key to find the value for.
     * @return The string value for the key. Returns an empty string if not found.
     */
    public String getValue(String key)
    {
        return getValue(key, "");
    }

    /**
     * Gets the string value for a specific key.
     * @param key The key to find the value for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return The string value for the key. Returns defaultValue when not found.
     */
    public String getValue(String key, String defaultValue)
    {
        if (!this.properties.containsKey(key)) {
            System.out.println("[CONFIG] Key not found: " + key);
        }
        return this.properties.getProperty(key, defaultValue);
    }

    /**
     * Gets the boolean value for a specific key.
     * @param key The key to find the value for.
     * @return The boolean value for the key. Returns false if not found.
     */
    public boolean getBoolean(String key)
    {
        return getBoolean(key, false);
    }

    /**
     * Gets the boolean value for a specific key.
     * @param key The key to find the value for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return The boolean value for the key. Returns defaultValue when not found.
     */
    public boolean getBoolean(String key, boolean defaultValue)
    {
        try
        {
            return (getValue(key, "0").equals("1")) || (getValue(key, "false").equals("true"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return defaultValue;
    }

    /**
     * Gets the int value for a specific key.
     * @param key The key to find the value for.
     * @return The int value for the key. Returns 0 if not found.
     */
    public int getInt(String key)
    {
        return getInt(key, 0);
    }

    /**
     * Gets the int value for a specific key.
     * @param key The key to find the value for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return The int value for the key. Returns defaultValue when not found.
     */
    public int getInt(String key, Integer defaultValue)
    {
        try
        {
            return Integer.parseInt(getValue(key, defaultValue.toString()));
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return defaultValue;
    }
}