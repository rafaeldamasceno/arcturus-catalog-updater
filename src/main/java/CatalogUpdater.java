import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class CatalogUpdater
{
    static HashMap<String, Pair<Integer, Integer>> alreadyAdded = new HashMap<String, Pair<Integer, Integer>>();
    static HashMap<String, Integer> catalogPages = new HashMap<String, Integer>();
    static Database database;
    static ConfigurationManager configurationManager;
    static long startTime;

    public static void main(String[] args)
    {
        try
        {
            startTime = System.currentTimeMillis();
            configurationManager = new ConfigurationManager("config.ini");
            database = new Database();
            loadData();

            File fXmlFile = new File("furnidata.xml");
            FileUtils.copyURLToFile(new URL("https://www.habbo.com/gamedata/furnidata_xml/0"), fXmlFile);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            final NodeList nList = doc.getElementsByTagName("furnitype");

            System.out.println("Parsing " + nList.getLength() + " items.");

            final int[] counter = {0, 0};

            System.out.println("--------------------------------------------------------------------------------");

            PreparedStatement stmt = database.prepare("INSERT INTO items_base (sprite_id, public_name, item_name, type, width, length, allow_walk, allow_sit, allow_lay) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            PreparedStatement clstmt = database.prepare("INSERT INTO catalog_items (page_id, item_ids, catalog_name, offer_id) VALUES (?, ?, ?, ?)");
            PreparedStatement cpstmt = database.prepare("INSERT INTO catalog_pages (caption_save, caption, icon_color, icon_image, visible, enabled, min_rank, club_only, order_num, page_layout, page_headline, page_teaser, page_special, page_text1, page_text2, page_text_details, page_text_teaser, vip_only) VALUES (?, ?, '1', '1', '1', '1', '1', '0', '1', 'default_3x3', '', '', '', '', '', '', '', '0')");

            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                final int finalTemp = temp;

                try
                {

                    Node nNode = nList.item(finalTemp);

                    if (alreadyAdded.containsKey(((Element) nNode).getAttribute("classname")))
                    {
                        if (!catalogPages.containsKey(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent()))
                        {
                            PreparedStatement statement = database.prepare("SELECT catalog_pages.id, catalog_pages.caption FROM catalog_pages INNER JOIN catalog_items ON catalog_items.page_id = catalog_pages.id WHERE catalog_items.catalog_name = ?");
                            statement.setString(1, ((Element) nNode).getAttribute("classname"));
                            ResultSet set = statement.executeQuery();

                            if (set.next())
                            {
                                PreparedStatement updateStatement = database.prepare("UPDATE catalog_pages SET caption_save = ? WHERE id = ? AND caption_save LIKE '' LIMIT 1");
                                updateStatement.setString(1, ((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent());
                                updateStatement.setInt(2, set.getInt("id"));
                                updateStatement.execute();

                                catalogPages.put(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent(), set.getInt("id"));
                                System.out.println("Updated " + set.getString("caption"));
                                updateStatement.getConnection().close();
                            }
                            set.close();
                            statement.close();
                            statement.getConnection().close();
                        }
                        if (!alreadyAdded.get(((Element) nNode).getAttribute("classname")).getValue().equals(Integer.valueOf(((Element) nNode).getAttribute("id")))) {
                            PreparedStatement update = database.prepare("UPDATE items_base SET sprite_id = ? WHERE item_name = ?");
                            update.setInt(1, Integer.valueOf(((Element) nNode).getAttribute("id")));
                            update.setString(2, ((Element) nNode).getAttribute("classname"));
                            update.execute();
                            update.close();
                            update.getConnection().close();
                            System.out.println("Updated " + ((Element) nNode).getAttribute("classname"));
                        }
                        continue;
                    }

                    stmt.setInt(1, Integer.valueOf(((Element) nNode).getAttribute("id")));
                    stmt.setString(2, ((Element) nNode).getElementsByTagName("name").item(0).getTextContent());
                    stmt.setString(3, ((Element) nNode).getAttribute("classname"));
                    if (((Element) nNode).getElementsByTagName("xdim").item(0) != null)
                    {
                        stmt.setString(4, "s");
                        stmt.setInt(5, Integer.valueOf(((Element) nNode).getElementsByTagName("xdim").item(0).getTextContent()));
                        stmt.setInt(6, Integer.valueOf(((Element) nNode).getElementsByTagName("ydim").item(0).getTextContent()));
                        stmt.setString(7, ((Element) nNode).getElementsByTagName("canstandon").item(0).getTextContent());
                        stmt.setString(8, ((Element) nNode).getElementsByTagName("cansiton").item(0).getTextContent());
                        stmt.setString(9, ((Element) nNode).getElementsByTagName("canlayon").item(0).getTextContent());
                    }
                    else
                    {
                        stmt.setString(4, "i");
                        stmt.setInt(5, 0);
                        stmt.setInt(6, 0);
                        stmt.setInt(7, 0);
                        stmt.setInt(8, 0);
                        stmt.setInt(9, 0);
                    }
                    stmt.execute();

                    ResultSet s = stmt.getGeneratedKeys();

                    if (!catalogPages.containsKey(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent()))
                    {
                        cpstmt.setString(1, ((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent());

                        String name = "";

                        String[] lineName = ((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent().split("_");
                        for (int i = 0; i < lineName.length; i++)
                        {
                            name += lineName[i].substring(0, 1).toUpperCase() + lineName[i].substring(1);

                            if (i + 1 != lineName.length)
                            {
                                name += " ";
                            }
                        }

                        cpstmt.setString(2, name);
                        cpstmt.execute();
                        ResultSet set = cpstmt.getGeneratedKeys();

                        if (set.next())
                        {
                            catalogPages.put(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent(), set.getInt(1));
                        }

                        System.out.println("Created catalog page \"" + name + "\" with ID: " + catalogPages.get(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent()));

                        ++counter[1];
                        set.close();

                    }

                    if (s.next())
                    {
                        clstmt.setInt(1, catalogPages.get(((Element) nNode).getElementsByTagName("furniline").item(0).getTextContent()));
                        clstmt.setInt(2, s.getInt(1));
                        clstmt.setString(3, ((Element) nNode).getAttribute("classname"));
                        clstmt.setInt(4, Integer.valueOf(((Element) nNode).getElementsByTagName("offerid").item(0).getTextContent()));
                        clstmt.execute();

                        System.out.println("Added item " + ((Element) nNode).getAttribute("classname"));
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                ++counter[0];
            }


            System.out.println("--------------------------------------------------------------------------------");
            System.out.println("New Catalog Pages Added: " + counter[1]);
            System.out.println("New Items Found: " + counter[0]);
            System.out.println(System.currentTimeMillis() - startTime + " MS!");
            System.out.println("--------------------------------------------------------------------------------");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void loadData()
    {
        try
        {
            PreparedStatement statement = database.prepare("SELECT id, caption_save FROM catalog_pages");
            ResultSet set = statement.executeQuery();

            while (set.next())
            {
                catalogPages.put(set.getString("caption_save"), set.getInt("id"));
            }

            set.close();
            statement.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        try
        {
            PreparedStatement statement = database.prepare("SELECT id, sprite_id, item_name FROM items_base");
            ResultSet set = statement.executeQuery();

            while(set.next())
            {
                alreadyAdded.put(set.getString("item_name"), new Pair<Integer, Integer>(set.getInt("id"), set.getInt("sprite_id")));
            }

            set.close();
            statement.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
}