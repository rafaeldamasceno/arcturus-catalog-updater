import com.zaxxer.hikari.HikariDataSource;

import java.sql.*;

public class Database
{
    private HikariDataSource dataSource;
    private Connection driverConnection;
    private Statement driverStatement;
    private DatabasePool databasePool;

    public Database()
    {
        boolean SQLException = false;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");

            databasePool = new DatabasePool();
            if (!databasePool.getStoragePooling())
            {
                SQLException = true;
                return;
            }
            this.dataSource = databasePool.getDatabase();
            this.driverConnection = this.dataSource.getConnection();
            this.driverStatement = this.driverConnection.createStatement();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            SQLException = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            SQLException = true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            SQLException = true;
        }
        finally
        {
        }
    }

    public PreparedStatement prepare(String query)
    {
        PreparedStatement statement = null;
        try
        {
            while(statement == null)
            {
                statement = this.dataSource.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                if(statement == null)
                {
                    this.databasePool.getStoragePooling();
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}

